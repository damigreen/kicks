# kicks

## Table of Contents

* [Personal Details](#Personal Details)
* [Description](#Description)
* [Technologies](#Technologies)
* [Installation](#Installation)

## Description

An online store were you get the best and the latest shoes for all occasions.


## Technologies

* [**React**](reactjs.org) - A JavaScript library for building user interfaces
* [**Nodejs**](nodejs.org/) - A JavaScript runtime built on Chrome's V8 JavaScript engine.
* [**Axios**](https://github.com/axios/axios) - A promise based HTTP client for the browser and nodejs
* [**Ant design**](https://ant.design/) - A design system for enterprise-level products. Create an efficient and enjoyable work experience

## Installation

```bash
$ git clone https://gitlab.com/damigreen/kicks.git
$ cd kicks && yarn
$ yarn start
```
