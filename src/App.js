import React, { useState, useEffect } from "react";
import './App.scss';
import productService from "./services/products";
import Header from "./components/header/";
import Products from "./components/products";
import Footer from "./components/footer/";
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import Login from "./components/login/";
import Signup from "./components/signup";
import useField  from './hooks/index';
import loginService from "./services/login";
import { useHistory, withRouter } from 'react-router-dom';



function App() {
  const [products, setProducts] = useState([]);
  const [user, setUser] = useState(null)

  useEffect(() => {
    productService.getAllProducts().then(response => setProducts(response));
    setUser(loginService.token);
  }, [setProducts]);


  
  return (
    <Router>
      <Switch>
        <div className="App">
          <Header user={user} products={products} />
          <Route exact path="/signup">
              <Signup />
          </Route> 
          <Route exact path="/login">
              <Login setUser={setUser} />
          </Route> 
          <Products products={products} />
          <Footer />
        </div>

      </Switch>
    </Router>
  );
}

export default App;
