import React from "react";
import "./Footer.scss";

function Footer() {

  return(
    <div className="product-footer">
      <div className="kicks-logo">KICKS LOGO</div>
      <div className="kicks-copyright">Copyright &copy; KICKS, Inc.</div>
      <div className="kicks-others">Privacy | Terms | Sitemap | Company</div>
    </div>
  )
}

export default Footer;
