import React from "react";
import Navigation from "./Navigation";
// import Hero from "./Hero";
import "./Header.scss";

function Header({ user, products}) {
  
  return (
    <div id="header">
      <Navigation user={user} products={products} />
      {/* <Hero /> */}
    </div>
  )
}

export default Header;
