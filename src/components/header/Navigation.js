import React, { useState } from "react";
import { Menu } from "antd";
import {
  Link,
  
} from "react-router-dom";
import { SearchOutlined } from "@ant-design/icons";

function Navigation({ user, products }) {
  const [value, setValue] = useState('')

  const handleChange = (e) => {
    setValue(e.target.value);
  }


  if (!user) {
    return (
      <div id="navigation">
        <Menu onClick mode="horizontal">
          <Menu.Item key="mail">
            <Link style={{ color: "green", fontSize: 24, fontWeight: 600 }} to="/">KICKS</Link>
          </Menu.Item>
          <Menu.Item key="men">
            <Link to="/men">Men</Link>
          </Menu.Item>
          <Menu.Item key="women">
            <Link to="/women">Women</Link>
          </Menu.Item>
          <Menu.Item key="kids">
            <Link to="/kids">Kids</Link>
          </Menu.Item>
          <Menu.Item style={{ float: "right" }} key="search">
            <SearchOutlined />
            <input style={{ height: "25px"}} placeholder="Search" value={value} onChange={handleChange} />
          </Menu.Item>
          <Menu.Item style={{ float: "right"}} key="login">
            <Link to="/login">Login</Link>
          </Menu.Item>
          <Menu.Item style={{ float: "right"}} key="signup">
            <Link to="/signup">Signup</Link>
          </Menu.Item>
        </Menu>
      </div>
    )

  } else {
      return (
        <div id="navigation">
          <Menu onClick mode="horizontal">
            <Menu.Item key="mail">
              <Link to="/">KICKS</Link>
            </Menu.Item>
            <Menu.Item key="men">
              <Link to="/men">Men</Link>
            </Menu.Item>
            <Menu.Item key="women">
              <Link to="/women">Women</Link>
            </Menu.Item>
            <Menu.Item key="kids">
              <Link to="/kids">Kids</Link>
            </Menu.Item>
            <Menu.Item style={{ float: "right" }} key="search">
              <SearchOutlined />
              <input style={{ height: "25px"}} placeholder="Search" />
            </Menu.Item>
            <Menu.Item style={{ float: "right"}} key="login">
              <Link to="/login">Login</Link>
            </Menu.Item>
            <Menu.Item style={{ float: "right"}} key="signup">
              <Link to="/signup">Signup</Link>
            </Menu.Item>
          </Menu>
        </div>
      )    
  }
}

export default Navigation;
