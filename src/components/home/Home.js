import React from "react";
import { Card } from "antd";
import "./Home.scss"
import { Link } from "react-router-dom"


function Home({ products }) {

  const productsList = products.map(prod => {
    const prodImage = prod.image;
    const prodImages = "images/main/new/"+prodImage+".PNG";

    return (
      <Link to={`product/${prod.id}`}>
        <div className="card">
          <Card className="card-style"
            hoverable
            style={{ width: 300 }}
            cover={<img src={prodImages} alt="Product images" />}
            >
            <p style={{ fontSize: "14px", fontWeight: 500 }}>{prod.name}</p>
            <p style={{ fontSize: "12px" }} >{prod.gender}</p>
            <p style={{ fontSize: "16px" }}>₦ {prod.price}</p>
          </Card>
        </div>
      </Link>
    )
  }); 

  return(
    <div className="home">
      <h2 className="home-header">New Releases</h2>
      <div>
        {productsList}
      </div>
    </div>
  )
}

export default Home;
