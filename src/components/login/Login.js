import React from "react";
import { Form, Input, Button } from 'antd';
import { useHistory, withRouter } from "react-router-dom";
import useField from "../../hooks/index";
import loginService from "../../services/login"
import "./Login.scss";

const tailLayout = {
  wrapperCol: {
    offset: 4,
    span: 16,
  },
};

const onFinishFailed = errorInfo => {
  console.log('Failed:', errorInfo);
};


function Login() {
  const username = useField("text").form;
  const password = useField("password").form;
  const history = useHistory();
  

  const handleLogin = ({ setUser }) => {
    console.log("God is good================");
    try {
      const loginObj = {
        username: username.value,
        password: password.value
      }

      loginService.login(loginObj).then(result => {
        loginService.setToken(result.token);
        console.log(result.token);
        setUser(result.token);
      });
      
      history.push("/");

    } catch(err) {
      console.log(err);
    }
  }
  

  return(
    <div>
      <div>
      <div type="primary">
      </div>
      <div className="login-page-box" >
          <h1 className='login-page-header'>SIGN IN</h1>
          <Form
            className='login-page-form'
            name="basic"
            initialValues={{
              remember: true,
            }}
            onFinish={handleLogin}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please input your firstName!',
                },
              ]}
              >
              <Input {...username} />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
              >
              <Input.Password {...password} />
            </Form.Item>

            <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button style={{backgroundColor: '#242b31', borderColor: '#242b31'}} type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>      
        </div>
      </div>

    </div>
  )
}

export default withRouter(Login);
