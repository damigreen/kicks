import React from "react";
import { Link } from "react-router-dom";
import { Card } from "antd";

function Men({ products }) {

  const menProducts = products.map(prod => {
    if (prod.gender === "men") {
      const prodImage = prod.image;
      const prodImages = "../images/main/new/"+prodImage+".PNG";

      return (
        <Link to={`product/${prod.id}`}>
        <div className="card">
          <Card className="card-style"
            hoverable
            style={{ width: 300 }}
            cover={<img src={prodImages} alt="Product images" />}
            >
            <p style={{ fontSize: "14px", fontWeight: 500 }}>{prod.name}</p>
            <p style={{ fontSize: "12px" }} >{prod.gender}</p>
            <p style={{ fontSize: "16px" }}>₦ {prod.price}</p>
          </Card>
        </div>
      </Link>
      )
    } 
  });

  console.log(products)
  return(
    <div>
      <div>Men Shoes</div>
      {menProducts}
    </div>
  )
}

export default Men;
