import React from "react";
import "./Product.scss";
import { Row, Col, Divider } from "antd";

function Product({ product }) {

  const productImage = `../../images/main/new/${product.image}.PNG`;

  return(
    <div id="product">
      <Row className="product-page">
        <Col>
          <img style={{ width: 500, height: 500 }} alt="product image" src={productImage} />
        </Col>
        <Col offset={3} className="product-detail">
          <h1 className="product-header">{product.name}</h1>
          <Divider />
          <p className="product-gender">{product.gender} shoe</p>
          <h1 className="product-price"> ₦ {product.price}</h1>
        </Col>
      </Row>
    </div>
  )
}

export default Product;
