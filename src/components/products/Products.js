import React, { Component } from "react";
import Men from "../men/";
import Women from "../women/";
import Kids from "../kids/";
import Home from "../home";
import {
  Route,
  Switch,
} from "react-router-dom";
import Login from "../login/";
import Product from "../product";


class Products extends Component {
  constructor(props){
    super(props);
  }


  render() {
    const { products = [] } = this.props;

    return(
      <Switch>
        <div id="products">
          <Route exact path="/">
            <Home products={products} />
          </Route>
          <Route path="/men">
            <Men products={products}/>
          </Route>
          <Route path="/women">
            <Women products={products} />
          </Route>
          <Route path="/kids">
            <Kids products={products} />
          </Route>
          <Route path="/product/:id" render={( {match}) => 
            <Product product={products.find(b => b.id === match.params.id)}/>
          }
          />
          {/* <Route path="/login">
            <Login />
          </Route> */}
        </div>
      </Switch>
    )
  }
}

export default Products;
