import React from "react";
import "./Signup.css";
import useField from "../../hooks/index";
import signupService from "../../services/signup";
import { useHistory } from 'react-router-dom';


function Signup() {
  const username= useField('text').form;
  const firstName= useField('text').form;
  const lastName= useField('text').form;
  const email= useField('email').form;
  const password= useField('password').form;
  const phoneNumber= useField('text').form;

  const history = useHistory();


  const signupUser = (e) => {
    e.preventDefault();

    try {
      const userObj = {
        firstName: firstName.value,
        lastName: lastName.value,
        email: email.value,
        username: username.value,
        phoneNumber: phoneNumber.value,
        password: password.value,
      };
  
      signupService.signup(userObj);
  
      // Set history to login
      history.push("/");

    } catch(e) {
      console.log(e);
    }
  };



  return(
    <div>
      <form onSubmit={signupUser}>
        <div className="container">
          <h1>Register</h1>
          <p>Please fill in this form to create an account.</p>
          <hr />

          <label for="firstName"></label>
          <input {...firstName} type="text" placeholder="First Name" required />
          {/* <input {...firstName} placeholder="First Name" required /> */}

          <label for="lastName"></label>
          <input {...lastName} type="text" placeholder="Last Name" required />

          <label for="email"></label>
          <input {...email} type="text" placeholder="Enter Email" required />

          <label for="username"></label>
          <input {...username} type="text" placeholder="Username" required />

          <label for="phoneNumber"><b>Phone Number</b></label>
          <input {...phoneNumber} type="text" placeholder="Phone Number" required />
 
          <label for="password"><b>Password</b></label>
          <input {...password} type="password" placeholder="Enter Password" required />

          <label for="psw-repeat"><b>Repeat Password</b></label>
          <input {...password} type="password" placeholder="Repeat Password" required />
 
          <hr />
          <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

          <button type="submit" class="registerbtn">Register</button>
        </div>

      </form>

        <div class="container signin">
          <p>Already have an account? <a href="#">Sign in</a>.</p>
        </div>


    </div>
    
  )
}

export default Signup;
