import React from "react";
import { Link } from "react-router-dom";
import { Card } from "antd";


function Women({ products }) {

  const womenProducts = products.map(prod => {
    if (prod.gender === "women") {
      const prodImage = prod.image;
      const prodImages = "../images/main/new/"+prodImage+".PNG";

      return (
        <Link to={`product/${prod.id}`}>
        <div className="card">
          <Card className="card-style"
            hoverable
            style={{ width: 300 }}
            cover={<img src={prodImages} alt="Product images" />}
            >
            <p style={{ fontSize: "14px", fontWeight: 500 }}>{prod.name}</p>
            <p style={{ fontSize: "12px" }} >{prod.gender}</p>
            <p style={{ fontSize: "16px" }}>₦ {prod.price}</p>
          </Card>
        </div>
      </Link>
      )
    } 
  });


  return(
    <div>
      <div>Women Product</div>
        {womenProducts}
    </div>
  )
}

export default Women;
