import axios from 'axios';

const baseUrl = 'http://localhost:3003/api/login';

let token = null;

const setToken = (newToken) => {
  token = `bearer ${newToken}`;
};


const login = async (credentials) => {
  const response = await axios({
    method: 'post',
    url: baseUrl,
    data: credentials
  });
  return response.data;
};

export default { login, setToken, token };
