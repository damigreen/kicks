import axios from "axios";

const baseUrl = "http://localhost:3003/api/products";

let token = null;

const setToken = (newToken) => {
  token = `bearer ${newToken}`;
};

const getAllProducts = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

const addProduct = async (newProduct) => {
  const config = {
    headers: { Authorization: token }
 };
 const response = await axios.post(`${baseUrl}`, newProduct, config);
 return response.data;
};

const updateProduct = async (id, productObj) => {
  const config = {
    headers: { Authorization: token }
 };
 const response = await axios.put(`${baseUrl}/${id}`, productObj, config);
 return response.data;
};

const delProduct = async (id) => {
  const config = {
    headers: { Authorization: token }
  }
  await axios.delete(`${baseUrl}/${id}`, config);
};

export default {
  addProduct,
  getAllProducts,
  updateProduct,
  delProduct
}
